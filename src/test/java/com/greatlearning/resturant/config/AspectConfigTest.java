package com.greatlearning.resturant.config;

import com.greatlearning.resturant.entity.AuditLog;
import com.greatlearning.resturant.repository.AuditLogRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.sql.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AspectConfigTest {

    @Mock
    AuditLogRepository auditLogRepository;

    @InjectMocks
    AspectConfig aspectConfig;

    void initUseDetailsAuthentication() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    void logUserBillGeneration() {
        initUseDetailsAuthentication();
        // when
        aspectConfig.logUserBillGeneration();
        // then
        Mockito.verify(auditLogRepository)
                .save(new AuditLog(any(),  " generated bill for his orders"));
    }

    @Test
    void logGenerateTodayBills() {
        // when
        aspectConfig.logGenerateTodayBills();
        // then
        Mockito.verify(auditLogRepository).save(new AuditLog(any(), "Generated bill for today "));
    }

    @Test
    void logFetTotalSalesForThisMonth() {
        // when
        aspectConfig.logFetTotalSalesForThisMonth();
        // then
        Mockito.verify(auditLogRepository)
                .save(new AuditLog(any(), "Generated bill for current month "));
    }
}