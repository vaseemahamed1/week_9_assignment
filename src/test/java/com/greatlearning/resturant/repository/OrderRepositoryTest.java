package com.greatlearning.resturant.repository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.greatlearning.resturant.entity.Orders;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class OrderRepositoryTest {

	Orders orderDetails;
	Date date = new Date(System.currentTimeMillis());

	@Autowired
	public OrderRepository orderRepository;

	@BeforeEach
	void setUp() {
		orderDetails = new Orders("user", 11, date, "biscut", 10);
		orderRepository.save(orderDetails);
	}

	@AfterEach
	public void destroyAll() {
		orderRepository.deleteAll();
	}

	@Test
	void save_and_fetch_order_details() {
		// when
		List<Orders> orders = orderRepository.findAll();
		// then
		Assertions.assertEquals(orders.size(), 1);
	}

	@Test
	void find_by_user_name() {
		// when
		List<Orders> order = orderRepository.findByUsername("user");
		// then
		Assertions.assertEquals(order.size(), 1);
	}

	@Test
	void find_by_user_name_negative_test() {
		// when
		List<Orders> order = orderRepository.findByUsername("user11");
		// then
		Assertions.assertEquals(order.size(), 0);
	}

	@Test
	void find_all_order_date() {
		// when
		List<Orders> order = orderRepository.findAllByOrderDate(date);
		// then
		Assertions.assertEquals(order.size(), 1);
	}

	@Test
	void find_all_order_date_negative_test() {
		// when
		Date d = new Date(System.currentTimeMillis());
		d.setYear(1990);
		List<Orders> order = orderRepository.findAllByOrderDate(d);
		// then
		Assertions.assertEquals(order.size(), 0);
	}

	@Test
	void find_all_by_order_date_month() {
		// when
		List<Orders> order = orderRepository.findAllByOrderDateMonth(1);
		// then
		Assertions.assertEquals(order.size(), 0);
	}

}
