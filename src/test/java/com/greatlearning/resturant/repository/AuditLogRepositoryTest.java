package com.greatlearning.resturant.repository;

import java.sql.Date;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.greatlearning.resturant.entity.AuditLog;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class AuditLogRepositoryTest {

	@Autowired
	private AuditLogRepository auditLogRepository;

	@Test
	void save_audit_log_to_audit_table() {
		AuditLog auditObject = new AuditLog(new Date(System.currentTimeMillis()), "auditDescription");
		auditLogRepository.save(auditObject);

		List<AuditLog> logs = auditLogRepository.findAll();
		Assertions.assertEquals(logs.size(), 1);
	}
}
