package com.greatlearning.resturant.repository;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.greatlearning.resturant.entity.Users;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class UserRepositoryTest {

	@Autowired
	private UserRepository userRepository;

	@BeforeEach
	void setUp() {
		Users user = new Users("testUser", "password", "ROLE_USER", "TRUE");
		userRepository.save(user);
	}

	@Test
	void find_by_user_name() {
		// when
		Users user = userRepository.getByUsername("testUser");
		// then
		Assertions.assertEquals(user.getRole(), "ROLE_USER");
	}

	@Test
	void find_by_user_name_negative_test() {
		// when
		Users user = userRepository.getByUsername("user11");
		// then
		Assertions.assertNull(user);
	}

	@Test
	void delete_by_user_name() {
		Users user = new Users("user1", "password", "ROLE_USER", "TRUE");
		userRepository.save(user);

		Users savedRecord = userRepository.getByUsername("user1");
		Assertions.assertEquals(savedRecord.getRole(), "ROLE_USER");

		// when
		 userRepository.deleteByUsername("user1");

		// then
		Users response = userRepository.getByUsername("user1");
		Assertions.assertNull(response);
	}

}
