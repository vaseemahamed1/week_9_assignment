package com.greatlearning.resturant.repository;

import java.sql.Date;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.greatlearning.resturant.entity.InventoryDetails;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class InventoryDetailsRepositoryTest {

	@Autowired
	private InventoryDetailsRepository inventoryDetailsRepository;

	@Test
	void save_and_fetch_invetory_details() {
		InventoryDetails inventoryDetails = new InventoryDetails(111, "biscut", 20);
		inventoryDetailsRepository.save(inventoryDetails);

		List<InventoryDetails> logs = inventoryDetailsRepository.findAll();
		// 5 elements already inserted by data.sql file , total count is 6
		Assertions.assertEquals(logs.size(), 6);
	}
}
