package com.greatlearning.resturant.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

import com.greatlearning.resturant.entity.BillDetailsDto;
import com.greatlearning.resturant.entity.InventoryDetails;
import com.greatlearning.resturant.entity.Orders;
import com.greatlearning.resturant.entity.UserDto;
import com.greatlearning.resturant.entity.Users;
import com.greatlearning.resturant.repository.InventoryDetailsRepository;
import com.greatlearning.resturant.service.OrdersService;
import com.greatlearning.resturant.service.UserService;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

	@Mock
	UserService userService;

	@Mock
	InventoryDetailsRepository inventoryDetailsRepository;

	@Mock
	OrdersService ordersService;

	@InjectMocks
	UserController userController;

	@Test
	public void get_user() throws Exception {
		// when
		String response = userController.home();
		// then
		Assertions.assertEquals("<h1> Welcome User controller</h1>", response);
	}

	@Test
	public void get_MenuItems() throws Exception {
		Mockito.when(inventoryDetailsRepository.findAll()).thenReturn(null);

		// when
		List<InventoryDetails> response = userController.getMenuItems();
		// then
		Assertions.assertNull(response);
		Mockito.verify(inventoryDetailsRepository).findAll();
	}

	@Test
	public void select_MenuItems() throws Exception {
		List<Integer> list = new ArrayList<Integer>();
		Optional<InventoryDetails> invetory = Optional.of(new InventoryDetails(1, "cake", 10));
		list.add(10);
		Mockito.when(inventoryDetailsRepository.findById(10)).thenReturn(invetory);

		// when
		String response = userController.selectMenuItems(list);
		// then
		Assertions.assertEquals("order added", response);
		Mockito.verify(inventoryDetailsRepository).findById(10);
	}

	@Test
	public void get_TotalBill() throws Exception {
		Authentication authentication = Mockito.mock(Authentication.class);
		SecurityContext securityContext = Mockito.mock(SecurityContext.class);
		Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		List<Orders> list = new ArrayList<Orders>();
		list.add(new Orders("user", 11, new Date(System.currentTimeMillis()), "biscut", 10));
		Mockito.when(ordersService.getByUsername(any())).thenReturn(list);

		// when
		BillDetailsDto response = userController.getTotalBill();
		// then
		Assertions.assertNotNull(response);
		Mockito.verify(ordersService).getByUsername(any());
	}
}
