package com.greatlearning.resturant.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.greatlearning.resturant.entity.UserDto;
import com.greatlearning.resturant.entity.Users;
import com.greatlearning.resturant.service.UserService;

@ExtendWith(MockitoExtension.class)
public class RegisterControllerTest {

	@Mock
	private UserService userService;

	@InjectMocks
	private RegisterController registerController;

	@Test
	public void register_user() throws Exception {

		Users user = new Users("usernew", "password", "ROLE_USER", "true");
		UserDto userDto = new UserDto("usernew", "password");

		Mockito.when(userService.createUser(userDto)).thenReturn(user);

		// when
		Users response = registerController.register(userDto);
		// then
		Assertions.assertEquals("password", response.getPassword());
		Mockito.verify(userService).createUser(userDto);
	}

}
