package com.greatlearning.resturant.controller;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.greatlearning.resturant.entity.BillDetailsDto;
import com.greatlearning.resturant.entity.TotalSalesDto;
import com.greatlearning.resturant.entity.UserDto;
import com.greatlearning.resturant.entity.Users;
import com.greatlearning.resturant.service.OrdersService;
import com.greatlearning.resturant.service.UserService;

@ExtendWith(MockitoExtension.class)
public class AdminControllerTest {
	
	@Mock
	UserService userService;

	@Mock
	OrdersService ordersService;

	@InjectMocks
	AdminController adminController;

	@Test
	public void get_user() throws Exception {

		Users user = new Users("usernew", "password", "ROLE_USER", "true");
		Mockito.when(userService.getUser("usernew")).thenReturn(user);

		// when
		Users response = adminController.getUser("usernew");
		// then
		Assertions.assertEquals("password", response.getPassword());
		Mockito.verify(userService).getUser("usernew");
	}

	@Test
	public void create_User() throws Exception {
		UserDto userDto = new UserDto("usernew", "password");
		Users user = new Users("usernew", "password", "ROLE_USER", "true");
		Mockito.when(userService.createUser(userDto)).thenReturn(user);

		// when
		Users response = adminController.createUser(userDto);
		// then
		Assertions.assertEquals(user, response);
		Mockito.verify(userService).createUser(userDto);
	}

	@Test
	public void update_User() throws Exception {
		UserDto userDto = new UserDto("usernew", "password");
		Users user = new Users("username", "pass", "ROLE_USER", "true");
		Mockito.when(userService.updateUser("username", userDto)).thenReturn(user);

		// when
		Users response = adminController.updateUser("username", userDto);
		// then
		Assertions.assertEquals(user, response);
		Mockito.verify(userService).updateUser("username", userDto);
	}

	@Test
	public void delete_User() throws Exception {
		// when
		String response = adminController.deleteUser("username");
		// then
		Assertions.assertEquals("user deleted", response);
		Mockito.verify(userService).deleteUser("username");
	}

	@Test
	public void generate_bills() throws Exception {
		// given
		List<BillDetailsDto> bills = Arrays.asList(new BillDetailsDto("user", Arrays.asList("cake", "biscuit"), 100),
				new BillDetailsDto("user1", Arrays.asList("cake", "biscuit"), 100));
		Mockito.when(ordersService.generateTodayBills()).thenReturn(bills);

		// when
		List<BillDetailsDto> response = adminController.generateTodayBills();
		// then
		Assertions.assertEquals(bills, response);
		Mockito.verify(ordersService).generateTodayBills();
	}

	@Test
	public void get_TotalSalesForMonth() throws Exception {
		// given
		TotalSalesDto salesDto = new TotalSalesDto("March", 100);
		Mockito.when(ordersService.getTotalSalesForThisMonth()).thenReturn(salesDto);

		// when
		TotalSalesDto response = adminController.getTotalSalesForMonth();
		// then
		Assertions.assertEquals(salesDto, response);
		Mockito.verify(ordersService).getTotalSalesForThisMonth();
	}

}
