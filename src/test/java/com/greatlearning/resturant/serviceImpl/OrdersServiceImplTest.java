package com.greatlearning.resturant.serviceImpl;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.greatlearning.resturant.entity.BillDetailsDto;
import com.greatlearning.resturant.entity.InventoryDetails;
import com.greatlearning.resturant.entity.Orders;
import com.greatlearning.resturant.entity.TotalSalesDto;
import com.greatlearning.resturant.repository.OrderRepository;
import com.greatlearning.resturant.serviceImpl.OrdersServiceImpl;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OrdersServiceImplTest {

	@Mock
	private OrderRepository orderRepository;

	@InjectMocks
	OrdersServiceImpl orderServiceImpl = new OrdersServiceImpl();

	Date date = new Date(System.currentTimeMillis());

	void initUseDetailsAuthentication() {
		Authentication authentication = Mockito.mock(Authentication.class);
		SecurityContext securityContext = Mockito.mock(SecurityContext.class);
		Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
	}

	@Test
	public void generate_today_bills() {
		// given
		List<Orders> orderList = Arrays.asList(new Orders("user", 10, date, "cake", 10),
				new Orders("user", 11, date, "biscut", 10));
		when(orderRepository.findAllByOrderDate(any())).thenReturn(orderList);

		// when
		List<BillDetailsDto> bills = orderServiceImpl.generateTodayBills();

		// then
		Assertions.assertNotNull(bills);
	}

	@Test
	public void get_total_SalesForThisMonth() {
		// given
		List<Orders> orderList = Arrays.asList(new Orders("user", 10, date, "cake", 10),
				new Orders("user", 11, date, "biscut", 10));
		when(orderRepository.findAllByOrderDateMonth(any())).thenReturn(orderList);

		// when
		TotalSalesDto totalSalesDto = orderServiceImpl.getTotalSalesForThisMonth();

		// then
		Assertions.assertNotNull(totalSalesDto);
		Assertions.assertEquals(totalSalesDto.getTotalSale(), 20);
	}

	@Test
	public void save_order() {
		// given
		initUseDetailsAuthentication();
		Optional<InventoryDetails> inventory = Optional.of(new InventoryDetails(1, "cake", 10));

		// when
		orderServiceImpl.saveOrder(SecurityContextHolder.getContext().getAuthentication(), 1, inventory);

		// then
		Mockito.verify(orderRepository).save(any());
	}

	@Test
	public void get_by_username() {
		// given
		List<Orders> orderList = Arrays.asList(new Orders("user", 10, date, "cake", 10),
				new Orders("user", 11, date, "biscut", 10));
		when(orderRepository.findByUsername(any())).thenReturn(orderList);
		initUseDetailsAuthentication();

		// when
		List<Orders> orders = orderServiceImpl.getByUsername(SecurityContextHolder.getContext().getAuthentication());

		// then
		Assertions.assertNotNull(orders);
	}

}
