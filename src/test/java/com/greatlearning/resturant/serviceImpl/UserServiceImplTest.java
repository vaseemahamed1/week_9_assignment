package com.greatlearning.resturant.serviceImpl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.greatlearning.resturant.entity.UserDto;
import com.greatlearning.resturant.entity.Users;
import com.greatlearning.resturant.repository.UserRepository;
import com.greatlearning.resturant.serviceImpl.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

	@Mock
	UserRepository userRepository;

	@Mock
	PasswordEncoder passwordEncoder;

	@InjectMocks
	UserServiceImpl userServiceImpl = new UserServiceImpl();

	@Test
	public void create_User() {
		// when
		userServiceImpl.createUser(new UserDto("user1", "password"));

		// then
		Mockito.verify(userRepository).save(any());
	}

	@Test
	public void get_user() {
		// given
		Users user = new Users("username", "password", "ROLE_USER", "true");
		when(userRepository.getByUsername("username")).thenReturn(user);

		// when
		Users response = userServiceImpl.getUser("username");

		// then
		Assertions.assertEquals(response.getPassword(), "password");
		Mockito.verify(userRepository).getByUsername(any());
	}

	@Test
	public void get_user_negative_case() {
		// given
		when(userRepository.getByUsername("username")).thenReturn(null);

		// when
		Users response = userServiceImpl.getUser("username");

		// then
		Assertions.assertNull(response);
		Mockito.verify(userRepository).getByUsername(any());
	}

	@Test
	public void delete_user() {
		// when
		userServiceImpl.deleteUser("username");

		// then
		Mockito.verify(userRepository).deleteByUsername("username");
	}

	@Test
	public void update_user() {
		// given
		Users user = new Users("username", "password", "ROLE_USER", "true");
		when(userRepository.getByUsername("username")).thenReturn(user);

		// when
		userServiceImpl.updateUser("username", new UserDto("user1", "pass1"));

		// then
		Mockito.verify(userRepository).getByUsername("username");
		Mockito.verify(userRepository).save(user);
	}

}
