package com.greatlearning.resturant.repository;

import com.greatlearning.resturant.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.sql.Date;
import java.util.List;

public interface OrderRepository extends JpaRepository<Orders, Integer> {
    List<Orders> findByUsername(String name);

    List<Orders> findAllByOrderDate(Date date);

    @Query(value = "select * from ORDERS where month(order_date)= ?1", nativeQuery = true)
    List<Orders> findAllByOrderDateMonth(Integer month);
}
