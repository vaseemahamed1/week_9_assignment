package com.greatlearning.resturant.controller;

import com.greatlearning.resturant.entity.BillDetailsDto;
import com.greatlearning.resturant.entity.InventoryDetails;
import com.greatlearning.resturant.entity.Orders;
import com.greatlearning.resturant.repository.InventoryDetailsRepository;
import com.greatlearning.resturant.service.OrdersService;
import com.greatlearning.resturant.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    InventoryDetailsRepository inventoryDetailsRepository;

    @Autowired
    OrdersService ordersService;


    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @GetMapping("/")
    public String home() {
        return ("<h1> Welcome User controller</h1>");
    }

    @GetMapping("/viewMenu")
    public List<InventoryDetails> getMenuItems() {
        return inventoryDetailsRepository.findAll();
    }

    @PostMapping("/selectMenu")
    public String selectMenuItems(@RequestParam List<Integer> menuItemsIds) {
        // get user name from spring security
        Authentication authentication = getAuthentication();
        // map menu items
        menuItemsIds.stream()
                .forEach(selectedItem -> {
                    Optional<InventoryDetails> item = inventoryDetailsRepository.findById(selectedItem);
                    if (item.isPresent()) {
                        ordersService.saveOrder(authentication, selectedItem, item);
                    }
                });
        return "order added";
    }

    @GetMapping("/getBill")
    public BillDetailsDto getTotalBill() {
        Authentication authentication = getAuthentication();
        List<String> items = new ArrayList<>();
        Integer totalAmt = 0;
        // create bill list
        for (Orders it : ordersService.getByUsername(authentication)) {
            items.add(it.getItemName());
            totalAmt = totalAmt + it.getPrice();
        }
        // bill builder object
        return new BillDetailsDto(authentication.getName(), items.stream().distinct().collect(Collectors.toList()), totalAmt);
    }

}
