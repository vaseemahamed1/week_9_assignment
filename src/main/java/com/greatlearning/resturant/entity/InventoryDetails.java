package com.greatlearning.resturant.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class InventoryDetails {

	@Id
	private Integer id;

	@Column(name = "item_name")
	private String itemName;

	@Column(name = "price")
	private int price;

	public InventoryDetails(Integer id, String itemName, int price) {
		this.id = id;
		this.itemName = itemName;
		this.price = price;
	}

	public InventoryDetails() {
		super();
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}
